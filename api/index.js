require("dotenv").config();
const PORT = process.env.PORT || 8080;
const KEY = process.env.USER_KEY;
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const morgan = require("morgan");

const mongoose = require("mongoose");

mongoose.connect(
  `mongodb+srv://antonmihail:${
    process.env.MONGO_PASS
  }@cluster0-pfbuy.gcp.mongodb.net/test?retryWrites=true`
);
const conn = mongoose.connection();

const Prj = conn.model("Project", require("./db/models/prjs"));

const instance = new Prj();
instance.save(err => {
  throw new err();
});

app.use(morgan("dev"));
app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
// parse application/json
app.use(bodyParser.json());

app.use(function(req, res, next) {
  if (req.headers.authorization != KEY) {
    return res.status(403).json({ error: "Incorrect Credentials" });
  }
  next();
});
// Mount all resource routes

// Home page
app.get("/", (req, res) => {
  res.json({
    status: 200,
    ready: true
  });
});

app.listen(PORT, () => {
  console.log("Example app listening on port " + PORT);
});
