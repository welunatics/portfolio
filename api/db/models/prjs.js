const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const Project = new Schema({
  author: { type: String, default: "Anton Lachmaniucu" },
  title: { type: String },
  description: { type: String },
  date: { type: Date },
  giturl: { type: String, match: /^https?:\/\/(www\.)?[a-z]{1,}.[a-z]{1,}/gm },
  imgurl: { type: String, match: /^https?:\/\/(www\.)?[a-z]{1,}.[a-z]{1,}/gm }
});

module.exports = Project;
